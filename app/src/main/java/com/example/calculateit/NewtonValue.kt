package com.example.calculateit

data class NewtonValue(
    var operation : String?,
    var expression : String?,
    var error : String?,
    val result : String?) {
}