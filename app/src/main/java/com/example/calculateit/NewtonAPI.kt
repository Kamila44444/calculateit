package com.example.calculateit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface NewtonAPI {
    @GET("/{operation}/{expression}")
    fun findResult(@Path("expression") expression : String, @Path("operation") operation : String) : Call<NewtonValue>

    @GET("/{operation}/{expression}")
    fun findZeroes(@Path("expression") expression : String, @Path("operation") operation : String) : Call<NewtonZeroesValue>
}