package com.example.calculateit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }

    fun accept(view: View){
        val retrofit = Retrofit.Builder()
            .baseUrl("https://newton.now.sh")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val newton = retrofit.create(NewtonAPI::class.java)

        if(operationSpinner.selectedItem.toString() != "zeroes") {
            val call = newton.findResult(exEditText.text.toString(), operationSpinner.selectedItem.toString())

            call.enqueue(object : Callback<NewtonValue> {
                override fun onFailure(call: Call<NewtonValue>, t: Throwable) {
                    resultTextView.text = "error"
                    Log.d("myDebug", t.message)
                }

                override fun onResponse(call: Call<NewtonValue>, response: Response<NewtonValue>) {
                    val body = response.body()
                    Log.d("myDebug", body.toString())
                    if (body != null) {
                        if(body.error == null) {
                            resultTextView.text = body.result
                        }
                        else{
                            resultTextView.text = body.error
                        }
                    } else {
                        Log.d("myDebug", "null")
                    }
                }
            })
        }
        else{
            val call = newton.findZeroes(exEditText.text.toString(), operationSpinner.selectedItem.toString())

            call.enqueue(object : Callback<NewtonZeroesValue> {
                override fun onFailure(call: Call<NewtonZeroesValue>, t: Throwable) {
                    resultTextView.text = "error"
                    Log.d("myDebug", t.message)
                }

                override fun onResponse(call: Call<NewtonZeroesValue>, response: Response<NewtonZeroesValue>) {
                    val body = response.body()
                    Log.d("myDebug", body.toString())
                    if (body != null) {
                        if(body.error == null) {
                            resultTextView.text = Arrays.toString(body.result)
                        }
                        else{
                            resultTextView.text = body.error
                        }
                    } else {
                        Log.d("myDebug", "null")
                    }
                }
            })
        }
    }
}
