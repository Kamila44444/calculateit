package com.example.calculateit

data class NewtonZeroesValue(
    var operation : String?,
    var expression : String?,
    var error : String?,
    val result : Array<Int?>) {
}